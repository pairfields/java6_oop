package main;
/*
1 Создать класс Товар, имеющий переменные имя, цена, рейтинг
2 Создать класс Категория, имеющий переменные имя и массив товаров
3 Создать несколько объектов класса Категория
4 Создать класс Basket, содержащий массив купленных товаров
5 Создать класс User, содержащий логин, пароль и объект класса Basket
6 Создать объект класса User
 */
public class Starter {
    public static void main(String[] args) {
        Product apple = new Product("яблоко", 5.40,5);
        Product pear = new Product("груша", 6.35, 6.78);
        Product apricot  = new Product("абрикос", 8.99, 9.2);
        Product[] fruits = {apple, pear, apricot};

        Product cucumber = new Product("кукумбер", 3.8,10);
        Product tomato = new Product("помидор", 7.2, 9.48);
        Product potato  = new Product("картошка", 6.97, 3.2);
        Product[] vegetables = {apple, pear, apricot};
        //3
        Category fruitsCategory = new Category("фрукты",fruits);
        Category vegetablesCategory = new Category("овощи",vegetables);
        //4
        Basket basket = new Basket(new Product[]{apple, pear, potato});
        //5
        User user = new User("login","password", basket);


    }
}
