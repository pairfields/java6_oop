package main;

public class User {
    private String userLogin;
    private String userPassword;
    private Basket userProducts;

    public User() {
    }

    public User(String userLogin, String userPassword, Basket userProducts) {
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.userProducts = userProducts;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Basket getUserProducts() {
        return userProducts;
    }

    public void setUserProducts(Basket userProducts) {
        this.userProducts = userProducts;
    }
}
